Plugin Cordova pour utilisation de NFC avec clef criptée sur une app Ionic
======


Permet de lire un numéro encodé sur le Secteur 1 bloc 0 d'une carte Mifare 13.56 Mhz

La clef peut être passé en paramètre